const weatherForm = document.querySelector('form')
const search = document.querySelector('input')
const messageone = document.querySelector('#m1')
const messagetwo = document.querySelector('#m2')
const messagethree=document.querySelector('#m3')


weatherForm.addEventListener('submit', (e) => {
    e.preventDefault()

    const location = search.value
    messageone.textContent='Loading...'
    messagetwo.textContent=''
    messagethree.textContent=''

    fetch('/weather?address=' + encodeURIComponent(location)).then((response) => {
        response.json().then((data) => {
            if (data.error) {

                messageone.textContent = 'error: '+data.error
            } else {
                messageone.textContent =  'Location: '+ data.location
                messagetwo.textContent=   'Forecast: '+data.forecast
                messagethree.textContent = 'Last update: ' +data.Timeupdated
            }
        })
    })
})