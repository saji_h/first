const path = require('path')
const express = require('express')
const hbs = require('hbs')
const geocode = require('./utils/geocode.js')
const forecast = require('./utils/forecast.js')

const app = express()
const port= process.env.PORT || 3000

//Define paths for Express config
const publicdir = path.join(__dirname, '../public')
const viewspath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

//Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views', viewspath)
hbs.registerPartials(partialsPath)

//Setup static directory to serve
app.use(express.static(publicdir))

app.get('', (req, res) => {
    res.render('index', {
        title: 'Weather',
        name: 'Sagee Hamati'
    })
})

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About Me',
        name: 'Sagee Hamati'
    })
})

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help',
        helpText: 'This is some helpful text.',
        name: 'Sagee Hamati'
    })
})

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'You must provid an address!'
        })
    }

    geocode(req.query.address, (error, { latitude, longtitude, location }= {}) => {
        if (error) {
            return res.send({ error })
        }

        forecast(latitude, longtitude, (error, forecastData,Timeupdated) => {
            if (error) {
                return req.send({ error })
            }

            res.send(
                {  
                    forecast: forecastData,
                    location,
                    address: req.query.address,
                    Timeupdated

                })
        })
    })
})

app.get('/products', (req, res) => {
    if (!req.query.search) {
        return res.send({
            error: 'You must provide a search!'
        })
    }

    res.send({
        products: [],
    })
})

app.get('/help/*', (erq, res) => {
    res.render('404', {
        title: 'Error 404',
        name: 'Sagee Hamati',
        errorText: 'Help article not found',
    })
})

app.get('*', (req, res) => {
    res.render('404', {
        title: 'Error 404',
        name: 'Sagee Hamati',
        errorText: 'Page not found'
    })
})

app.listen(port, () => {
    console.log('server is up on port' + port)
})