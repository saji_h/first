const request = require("request");
//forecast takes latitude,longtitude
const forecast = (latitude,longtitude, callback) => {
    const url = 'https://api.weatherapi.com/v1/forecast.json?key=8b0afbefdfa3453e91a193030221203&q=' + encodeURIComponent(latitude) + ',' + encodeURIComponent(longtitude) + '&days=2&lang=en'
    request({ url , json: true }, (error, {body}={}) => {
        if (error) {
            callback('Unable to connect to weather service!', undefined)
        }
        else if (body.error) {
            callback(response.body.error.message,undefined)
        }

        else {
            const data = {
            temp : body.current.temp_c,
            feelslike:body.current.feelslike_c,
            day : body.current.condition.text,
            Timeupdated:body.current.last_updated
        }
        const {Timeupdated} = data

            callback(undefined,data.day+ ' It is currently ' + data.temp +'C degrees out and it feels like '+ data.feelslike +'C degrees.',Timeupdated)
        }
    })
}
module.exports= forecast
